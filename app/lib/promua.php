<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 06.08.2019
 * Time: 21:29
 */

$json = '{
  "order_id": 115,
  "user": 99,
  "delivery_name": "sdbgdasgdasg",
  "delivery_email": "dasgdsga@zz.cc",
  "delivery_phone": "+38 (012) 421-42-14",
  "delivery_city": "Киев",
  "delivery_address": "",
  "delivery_type": {
    "id": 42,
    "title": "Самовывоз"
  },
  "delivery_price": 0,
  "comment": "",
  "payment_type": {
    "id": 13,
    "title": "Наличными"
  },
  "payed": 0,
  "total_default": 31799,
  "total_sum": 31799,
  "total_quantity": 1,
  "discount_percent": 0,
  "discount_value": 0,
  "coupon_code": "",
  "coupon_percent": 0,
  "coupon_discount_value": 0,
  "coupon_type": 0,
  "stat_status": 1,
  "stat_created": "2016-12-05 15:46:40",
  "currency": "UAH",
  "products": [
    {
      "title": "MacBook Air 11.6\" 128 ГБ, Зеленый",
      "article": "MJVM2UAA",
      "price": 31799,
      "quantity": 1,
      "discount_marker": "DISCOUNT_CARD",
      "total_price": 31799
    }
  ]
}';
$headers = array (
    'Content-Type: application/json'
);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'].'/order/created?id='.$_GET['id']);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch,CURLOPT_PUT,true);

if (!empty($body)) {
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
}
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);
echo $result;
return json_decode($result, true);