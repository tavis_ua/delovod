<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 18.07.2019
 * Time: 10:52
 */
ini_set('display_errors', 1);
error_reporting(E_ALL);

function debug($val){
    echo '<pre>';
    var_dump($val);
    exit();
}