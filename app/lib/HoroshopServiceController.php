<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 01.08.2019
 * Time: 12:12
 */

namespace app\lib;

use app\core\Controller;
use app\models\Horoshop;


class HoroshopServiceController
{

    public function indexAction(){

        $out = '<table class="table table-sm table-striped">
            <thead>
                <tr class="table-primary">
                    <th>Артикул</th>
                    <th>Название</th>
                    <th>Раздел</th>
                    <th>Остаток "Хорошоп"</th>
                    <th>Остаток "Деловод"</th>
                    <th colspan="3">&nbsp;</th>
                </tr>
            </thead>
            <tbody>';
        $out .= '</tbody></table>';
        $data = [
            'content'=>$out,
            'id_usr'=>isset($_REQUEST['id'])?$_REQUEST['id']:0
        ];
        $this->view->render('Панель "Хорошоп"', $data);
    }


    /*Передаем значение остатков на хорошоп
     * */
    function saveqtyAction($params){
        foreach ($params as $key=>$qty) {
            $data = [
                "token" => $_SESSION['user_info']->horoshop_token,
                "url" => $_SESSION['user_info']->horoshop_project . '/api/catalog/importResidues',
                "products" => [
                    ["article" => $key, "warehouse" => "office", "quantity" => $qty],
                ]
            ];
            $result = json_decode($this->Request($data, '', array('Content-Type: application/json')));
        }
        return true;
    }
    public function getHoroshopPaymentMethod(){
        $data = ['url'=>$_SESSION['user_info']->horoshop_project.'/api/payment/export/',
            'token'=>$_SESSION['user_info']->horoshop_token,
            'method'=>'get',
        ];
        $payment_methods = json_decode($this->Request($data, '',  array('Content-Type: application/json')))->response->payment;
        return $payment_methods;
    }

    /*Список способов доставки
     * */
    public function getDeliveryMethod(){
        $data = ['url'=>$_SESSION['user_info']->horoshop_project.'/api/delivery/export/',
            'token'=>$_SESSION['user_info']->horoshop_token,
        ];
        $methods = json_decode($this->Request($data, '',  array('Content-Type: application/json')))->response;
        if(isset($methods->delivery)) {
            $out = [];
            foreach ($methods->delivery as $item){
                $out[$item->id]['ru']           = $item->title->ru;
                $out[$item->id]['uk']           = $item->title->ua;
                $out[$item->id]['en']           = $item->title->en;
            }
            return $out;
        }else{
            echo '<script>alert("'.$methods->message.'");</script>';
            exit();
        }
    }
    /*Список категорий
     * */
    public function getCategories(){
        $data = ['url'=>$_SESSION['user_info']->horoshop_project.'/api/pages/export/',
            'token'=>$_SESSION['user_info']->horoshop_token,
        ];
        $categories = json_decode($this->Request($data, '',  array('Content-Type: application/json')))->response;
        if(isset($categories->pages)) {
            $out = [];
            foreach ($categories->pages as $item){
                $out[$item->id]['parent']       = $item->parent;
                $out[$item->id]['ru']           = $item->title->ru;
                $out[$item->id]['uk']           = $item->title->ua;
                $out[$item->id]['en']           = $item->title->en;
            }
            return $out;
        }else{
            echo '<script>alert("'.$categories->message.'");</script>';
            exit();
        }
    }

    /* Список заявок
     * */
    public function getOrders(){
        $data = ['url'=>$_SESSION['user_info']->horoshop_project.'/api/orders/get/',
            'token'=>$_SESSION['user_info']->horoshop_token,
        ];
        $horoshop = json_decode($this->Request($data, '',  array('Content-Type: application/json')))->response;
        $out = [];
        foreach ($horoshop->orders as $item){
            $out[$item->order_id] = $item;
        }
        return $out;
    }

    /*Список способов оплаты
     * */
    public function getPaymentMethod(){
        $data = ['url'=>$_SESSION['user_info']->horoshop_project.'/api/payment/export/',
            'token'=>$_SESSION['user_info']->horoshop_token,
        ];
        $horoshop = json_decode($this->Request($data, '',  array('Content-Type: application/json')))->response;
        $out = [];
        foreach ($horoshop as $item){
            $out[$item->article] = $item->quantity;
        }
        return $out;
    }

    /* Список товаров с текущими остатками
     * */
    public function getBalance($product_list = [], $balance_only = true){
        $data = ['url'=>$_SESSION['user_info']->horoshop_project.'/api/catalog/export/',
            'token'=>$_SESSION['user_info']->horoshop_token,
        ];
        if(count($product_list)){
            $data["expr"] = ["article"=> $product_list];
        }
        $horoshop = json_decode($this->Request($data, '',  array('Content-Type: application/json')))->response->products;
        $out = [];
        foreach ($horoshop as $item){
            if($balance_only) {
                $out[$item->article] = $item->quantity;
            }else{
                $out[$item->article] = ['name'=>['ru'=>$item->mod_title->ru, 'uk'=>$item->mod_title->ua], 'parent'=>$item->parent->id];
                $out[$item->article]['quantity'] = $item->quantity;
                $out[$item->article]['price'] = $item->price;
            }
        }
        return $out;
    }
    /* Метод запросов
     * */
    public function Request($param = ['url'=>'/'], $postfields_name = '', $httpheader = array()){

        if($curl=curl_init())
        {
            curl_setopt($curl,CURLOPT_URL,$param['url']);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheader);
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            if(!isset($param['method']) || $param['method'] == 'post')
                curl_setopt($curl,CURLOPT_POST,true);
            curl_setopt($curl,CURLOPT_POSTFIELDS, $postfields_name.json_encode($param));
            $out=curl_exec($curl);
            curl_close($curl);
        }
        return $out;
    }
}