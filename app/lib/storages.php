<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 28.07.2019
 * Time: 14:55
 */

$out = [];
if(isset($_REQUEST['key']) && !empty($_REQUEST['key'])){
    $packet['key']=$_REQUEST['key'];
    $packet['version']="0.15";

    $packet['action']="request";
    $packet['params']['from']="catalogs.storages";
    $packet['params']['fields']=["id"=>"id", "name"=>"name", "code"=>"code"];
    require 'DB.php';
    $db = new app\lib\DB();
    if($curl=curl_init())
    {
        if(empty($_SESSION['user_info']->delovod_gate)){
            $sql = "select delovod_version from users where delovod_authorize =:delovod_authorize and active = 1";
            $data = ['delovod_authorize'=>$_REQUEST['key']];
            $delovod_version = $db->getOne($sql, $data)->delovod_version;
            $_SESSION['user_info'] = new stdClass();
            if($delovod_version == 9){
                $_SESSION['user_info']->delovod_gate = 'https://delovod.ua/api/';
            }elseif ($delovod_version == 10){
                $_SESSION['user_info']->delovod_gate = 'https://api.delovod.ua';
            }
        }
        curl_setopt($curl,CURLOPT_URL,$_SESSION['user_info']->delovod_gate);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_POST,true);
        curl_setopt($curl,CURLOPT_POSTFIELDS,"packet=".json_encode($packet));
        $out=json_decode(curl_exec($curl));
        curl_close($curl);
    }
    //Если есть информация о складах - ставлю метки на тех складах, по которым ведутся рассчеты остатков
    if(count($out) > 0){
        $sql = "select delovod_storage from users where delovod_authorize =:delovod_authorize and active = 1";
        $data = ['delovod_authorize'=>$_REQUEST['key']];
        $storage_list = trim($db->getOne($sql, $data)->delovod_storage, '[]');
        $storage_list = str_replace('"', '', $storage_list);
        $storage_list = explode(',', $storage_list);
        foreach ($out as $key=>$item){
            if(in_array($item->id, $storage_list)){
                $out[$key]->storage_on = 'on';
            }
        }
    }
    echo json_encode($out);
}

