<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 07.08.2019
 * Time: 08:58
 */

namespace app\lib;
define('AUTH_TOKEN', '');  // Your authorization token
define('HOST', 'my.prom.ua');  // e.g.: my.prom.ua, my.tiu.ru, my.satu.kz, my.deal.by, my.prom.md
use app\models\Prom;

class PromuaServiceController
{

    /*Запрос к prom.ua
     *@param string     //метод передачи данных
     *@param string     //url запроса к методам
     *@param array     //массив данных
     *@result string    //результат запроса*/
    public function make_requestAction($method, $url, $body = null) {
        $headers = array (
            'Authorization: Bearer ' . $_SESSION['user_info']->promua_authorize,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://' . HOST . '/api/v1'.$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if (strtoupper($method) == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
        }
        if (!empty($body)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        return json_decode($result, true);
    }
    /*Возвращает список методов оплаты
     * */
    public function getPromuaPaymentMethod(){
        $payment_methods = $this->make_requestAction('GET', '/payment_options/list', NULL);
        return $payment_methods;
    }

    /*Установка доступности товара
     * @param array артикулы товаров с кол-вом
     * @result bit*/
    public function setProductStatus($products = []){
        if(count($products) == 0)
            return;
        $prom_db = new Prom();
        $product_list = $prom_db->getProductsList(array_keys($products));
        foreach ($product_list as $product){
            if(isset($products[$product->prom_articul])){

                if(empty($products[$product->prom_articul])){
                    $data = [array( "id"=> (int)$product->prom_id, "status"=> "not_on_display")];
                }else{
                    $data = [array( "id"=> (int)$product->prom_id, "status"=> "on_display")];
                }
            }else{
                $data = [array( "id"=> (int)$product->prom_id, "status"=> "not_on_display")];
            }
            $this->make_requestAction('POST', '/products/edit', $data);

        }
    }
}