<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 18.07.2019
 * Time: 12:31
 */

namespace app\lib;
use PDO;

class DB
{
    protected $db;

    function __construct()
    {
        $config = require $_SERVER['DOCUMENT_ROOT'].'/app/config/config.php';
        $dsn = 'mysql:host='.$config['db_host'].';dbname='.$config['db_name'];
        $username = $config['db_user'];
        $password = $config['db_pass'];
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );

        $this->db = new PDO($dsn, $username, $password, $options);
    }

    public function query($sql, $params){
        $stmt = $this->db->prepare($sql);
            if(!empty($params)) {
                foreach ($params as $key=>$param) {
                    $stmt->bindValue(':'.$key, $param, PDO::PARAM_STR);
                }
            }
        if(!$stmt->execute()) {
            return false;
        }else {
            return $stmt;
        }
    }
    /*Возвращает все результаты запроса
     * @param string
     * @param array
     * @result array*/
    public function getAll($sql, $params = []){
        $result = $this->query($sql, $params);
        $out = [];
        while ($obj = $result->fetchObject()){
            $out[] = $obj;
        }
        return $out;
    }

    /*Возвращает одну строку запроса
     * @param string
     * @param array
     * @result array*/
    public function getOne($sql, $params = []){
        $result = $this->query($sql, $params);
        if(!$result){
            var_dump($this->db);
            exit();
        }
        return $result->fetchObject();
    }

    /*Добавляет запись в таблицу
     * @param string
     * @param array
     * @result array*/
    public function addItem($sql, $params = []){
        $result = $this->query($sql, $params);
        if(!$result){
            var_dump($this->db);
            exit();
        }
        return $result;
    }

    /*Удаляет запись с таблицы
     * @param string
     * @param array
     * @result array*/
    public function deleteItem($sql, $params = []){
        $result = $this->query($sql, $params);
        if(!$result){
            var_dump($this->db);
            exit();
        }
        return $result;
    }

    /*Обновляет запись таблицы
     * @param string
     * @param array
     * @result array*/
    public function updateItem($sql, $params = []){
        $result = $this->query($sql, $params);
        if(!$result){
            var_dump($this->db);
            exit();
        }
        return $result;
    }
}