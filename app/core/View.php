<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 21.07.2019
 * Time: 12:11
 */

namespace app\core;


class View
{
    public $path;   //Путь к виду;
    public $layout = 'default'; //Путь к шаблону;
    public $route;

    public function __construct($route){
        $this->path = $route['controller'].'/'.$route['action'];
        $this->route = $route;
    }

    public function render($title, $data=[]){
        extract($data);
        if(file_exists('app/views/'.$this->path.'.php')) {
            ob_start();
            require_once 'app/views/' . $this->path . '.php';
            $content = ob_get_clean();
        }else{
            self::errorCode(404);
        }
        if(file_exists('app/views/layouts/'.$this->layout.'.php')) {
            require_once 'app/views/layouts/' . $this->layout . '.php';
        }else{
//            die('файл '.$this->layout.'.php не найден');
            self::errorCode(404);
        }

    }

    public static function errorCode($code){
//        http_response_code($code);
        require_once 'app/views/errors/'.$code.'.php';
        exit();
    }
}