<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 21.07.2019
 * Time: 18:14
 */

namespace app\core;

use app\lib\DB;

abstract class Model
{
    public $db;
    function __construct()
    {
        $this->db = new DB();
    }

    public function userInfo(){
        if(count($_REQUEST) == 0 && !isset($_SESSION['user_info']->id)){
            return false;
        }
        //Определяю параметры авторизации
        if(isset($_REQUEST['id']) && !empty($_REQUEST['id']) || !isset($_SESSION['user_info']->id)) {
            $sql = "select * from users where id=:id";
            $id_usr = $_REQUEST['id'];
            $data = ['id' => $id_usr];
            return $this->db->getAll($sql, $data)[0];
        }
        return false;
    }

    public function getPromuaGoods($articels){
        foreach ($articels as $key => $articul){
            if(empty($articul)){
                $articels[$key] = '0';
            }
        }
        $sql = 'SELECT prom_articul, presence from prom_goods where prom_articul in ("'.implode('","', $articels).'")';
        $return = $this->db->getAll($sql);
        $out = [];
        foreach ($return as $item){
            $out[$item->prom_articul] = $item->presence;
        }
        return $out;
    }

    public function getUnSynchronArticles(){
        $sql = "select not_sinchronize_articul from users where id = ".$_SESSION['user_info']->id;
        $result = $this->db->getOne($sql);
        return explode(',', $result->not_sinchronize_articul);
    }

}