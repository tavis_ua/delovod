<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 21.07.2019
 * Time: 12:17
 */

namespace app\core;
use app\controllers\AccountController;
use app\lib\DB;
use app\lib\PromuaServiceController;
use app\lib\HoroshopServiceController;

abstract class Controller
{
    public $route;
    public $view;
    public $db;
    public $model;
    public $horoshop_icon;
    public $user_info;
    public $promua_service;
    public $horohop_service;

    public function __construct($route){
        $this->route = $route;
        $this->view = new View($route);
        $this->db = new DB();
        $this->horohop_service= new HoroshopServiceController();
        $this->promua_service = new PromuaServiceController();
        $this->model = $this->loadModel($route['controller']);
    }
    public function authorizationAction(){

        $this->user_info = $this->model->userInfo();
        if($this->user_info->delovod_version == 9){
            $this->user_info->delovod_gate = 'https://delovod.ua/api/';
        }elseif ($this->user_info->delovod_version == 10){
            $this->user_info->delovod_gate = 'https://api.delovod.ua';
        }
        if(!isset($_SESSION['horoshop_authorization']['time']) || time() - $_SESSION['horoshop_authorization']['time'] > 600 || empty($_SESSION['user_info']->horoshop_token)) {
            $_SESSION['horoshop_authorization']['time'] = time();

            //Авторизируюсь на хорошопе
            $data = ['url' => $this->user_info->horoshop_project . '/api/auth', 'login' => $this->user_info->horoshop_login, 'password' => $this->user_info->horoshop_pass];
            $out = json_decode($this->Request($data, '', array('Content-Type: application/json')));

            if($out->status == 'HTTP_ERROR' || empty($out)){
                if(empty($out)) {
                    $additionMess = 'Возможно не корректно указано доменное имя проекта';
                }elseif (isset($out->response->code)){
                    $additionMess='Код ошибки '.$out->response->code;
                }
                echo '<script>alert("Ошибка авторизации на Хорошопе. '.$additionMess.'")</script>';
                return;
            }


            if(isset($out->response->token)) {
                $this->user_info->horoshop_token = $out->response->token;
            }
        }else{
            foreach ($_SESSION['user_info'] as $key=>$param){
                if(substr($key, 0, strlen('horoshop')) == 'horoshop') {
                    $this->user_info->$key = $param;
                }
            }
        }
        $_SESSION['user_info'] = $this->user_info;
    }

    /*Список товаров
     * */
    public function goodsAction($only_different = false){
        $perPage = $only_different?500:50;
        $this->authorizationAction();
        if (!isset($_SESSION['user_info']->horoshop_token)|| empty($_SESSION['user_info']->horoshop_token)){
            exit();
        }
        if(empty($this->user_info->delovod_storage)){
            echo 'Необходимо указать в настройках аккаунта склад для анализа остатков';
            exit();
        }
        unset($_SESSION['products'][$_REQUEST['id']]);
        if(!isset($_SESSION['products'][$_REQUEST['id']]) || empty($_SESSION['products'][$_REQUEST['id']])) {
            //Загружаю товары с деловода
            $post = [
                "url" => $_SESSION['user_info']->delovod_gate,
                "version" => "0.15",
                "key" => $this->user_info->delovod_authorize,
                "action" => "request",
                "params" => [
                    "from" => "catalogs.goods",
                    "fields" => ["id" => "good",
                        "productNum" => "productNum",
                        "name" => "name",
                    ],
                ]
            ];
            $products = json_decode($this->Request($post, 'packet='));
            if(!$products){
                echo '<script>alert("Ошибка подключения к Деловоду. Попробуйте обновить страницу");</script>';
                exit();
            }
            //удаляю название групп
            foreach ($products as $key => $product) {
                if (empty($product->productNum)) {
                    unset($products[$key]);
                }
            }
            foreach ($products as $product) {
                $_SESSION['products'][$_REQUEST['id']]['delovod'][$product->productNum]['name'] = $product->name;
            }
            $goods = [];
            foreach ($products as $product){
                if(is_array($product)) {
                    $goods[] = $product['productNum'];
                }else{
                    if(count($goods) == count($products)-1){
                        $test = true;
                    }
                    $goods[] = $product->productNum;
                }
            }

            //Загружаю остатки с деловода
            $post = [
                "url" => $_SESSION['user_info']->delovod_gate,
                "version"=>"0.15",
                "key"=>$this->user_info->delovod_authorize,
                "action"=>"request",
                "params"=>[
                    "from"=>[
                        "type"=>"balance",
                        "register"=>"goods",
                        "date"=>date('Y-m-d 00:00:00'),

                    ],
                    "filters"=>[
                        ["alias"=>"productNum",
                            "operator"=>"IL",
                            "value"=>$goods],
                        ["alias"=>"storage",
                            "operator"=>"IL",
                            "value"=>json_decode($this->user_info->delovod_storage)]
                    ],
                    "fields"=>["good"=>"good",
                        "good.code"=>"code",
                        "good.productNum"=>"productNum",
                        "storage"=>"storage",
                        "qty"=>"qty"],
                ]
            ];
            $storageQty = json_decode($this->Request($post, 'packet='));
            if(!isset($storageQty->error) || empty($storageQty->error)) {
                foreach ($storageQty as $key => $item) {
                    $_SESSION['products'][$_REQUEST['id']]['delovod'][$item->productNum]['qty'] =
                        isset($_SESSION['products'][$_REQUEST['id']]['delovod'][$item->productNum]['qty']) ? $_SESSION['products'][$_REQUEST['id']]['delovod'][$item->productNum]['qty'] + $item->qty : $item->qty;
                }
            }else{
                echo '<script>alert(\'Ошибка сервиса "Деловод" '.$storageQty->error.'\')</script>';
                exit();
            }
        }
//      Загружаю список товаров на хорошопе
        $page = (int)(isset($_REQUEST['page'])?$_REQUEST['page']:1)-1;
        $numRow = 0;
        $articles = [];
        $horoshop = [];//Список товаров хорошопа с информацией о наличии
        $goodsCount = count(array_keys($_SESSION['products'][$_REQUEST['id']]['delovod']));
        foreach (array_keys($_SESSION['products'][$_REQUEST['id']]['delovod']) as $key=>$article) {
            if ($numRow >= $page * $perPage && $numRow <= (($page + 1) * $perPage - 1)||$only_different) {
                $articles[] = $article;
            }
            $numRow++;
            /*Если количество артикулов = кол-ву на страницу или 500 в случае, если показывать только различия
             *выполняю запрос остатков хорошопа */
            $horoshop = [];
            if(count($articles) == $perPage && !$only_different || $only_different && (count($articles)%$perPage == 0 || $key == $goodsCount - 1)){
                if (isset($_SESSION['user_info']->horoshop_token) && !empty($_SESSION['user_info']->horoshop_token)) {
                    $data = ['url' => $_SESSION['user_info']->horoshop_project . '/api/catalog/export/',
                        'token' => $_SESSION['user_info']->horoshop_token,
                        "expr" => ["article" => $articles],
                    ];
                    $result = json_decode($this->Request($data, '', array('Content-Type: application/json')))->response;
                    if($result) {
                        if(!isset($result->products)){
                            echo '<script>alert("'.$result->message.'");</script>';
                            exit();
                        }
                        $horoshop = $result->products;
                    }
                }
                foreach ($horoshop as $key => $item) {
                    $_SESSION['products'][$_REQUEST['id']]['horoshop'][$item->article]['qty'] =
                        isset($_SESSION['products'][$_REQUEST['id']]['horoshop'][$item->article]['qty']) ? $_SESSION['products'][$_REQUEST['id']]['horoshop'][$item->article]['qty'] + $item->quantity : $item->quantity;
                }
                //Если установлен флаг - только отличия, обнуляю список артиклей и иду дальше. В противном случае прерываю перебор
                if ($only_different){
                    $articles = [];
                }else{
                    break;
                }
            }
        }
        if($only_different){
            $articles = array_keys($_SESSION['products'][$_REQUEST['id']]['delovod']);
        }
        $out = [];
        if(isset($_SESSION['products'][$_REQUEST['id']]) && count($_SESSION['products'][$_REQUEST['id']]) > 0) {
            //Загружаю последнюю информацию с prom.ua
            $promua = $this->model->getPromuaGoods($articles);
            foreach ($_SESSION['products'][$_REQUEST['id']]['delovod'] as $key => $item) {
                if (in_array($key, $articles)) {
                    $error = (isset($_SESSION['products'][$_REQUEST['id']]['horoshop'][$key])?$_SESSION['products'][$_REQUEST['id']]['horoshop'][$key]['qty']:0) != (isset($item['qty']) ? (int)$item['qty'] : 0);
                    $horoshop_qty = isset($_SESSION['products'][$_REQUEST['id']]['horoshop'][$key])?$_SESSION['products'][$_REQUEST['id']]['horoshop'][$key]['qty']:0;
                    $delovod_qty = isset($item['qty']) ? (int)$item['qty'] : 0;
                    if(!$only_different || $only_different && $horoshop_qty != $delovod_qty)
                    $out[] = [
                        'class' => $error ? 'class="error"' : '',   //Класс
                        'key' => $key,                        //Артикль
                        'title' => $item['name'],            //Название
                        'horoshop_qty' => $horoshop_qty,       //Остаток хорошопа
                        'delovod_qty' => $delovod_qty,//Остаток деловод
                        'promua_presense' => !isset($promua[$key]) ? '': $promua[$key] == 'on_display'?'1':0
                    ];

                }
            }
        }
        return $out;
    }

    public function loadModel($name){
        $path = 'app\models\\'.ucfirst($name);
        if(class_exists($path)){
            return new $path;
        }
        echo $path;
    }

    /* Метод запросов
     * */
    public function Request($param = ['url'=>'/'], $postfields_name = '', $httpheader = array()){

        if($curl=curl_init())
        {
            curl_setopt($curl,CURLOPT_URL,$param['url']);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheader);
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            if(!isset($param['method']) || $param['method'] == 'post') {
                curl_setopt($curl, CURLOPT_POST, true);
            }
            curl_setopt($curl,CURLOPT_POSTFIELDS, $postfields_name.json_encode($param));
            $out=curl_exec($curl);
            curl_close($curl);
        }
        return $out;
    }
}