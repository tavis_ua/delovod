<?php

/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 18.07.2019
 * Time: 11:56
 */
namespace app\core;
class Route
{
    protected $routes = [];
    protected $params = [];

    function __construct()
    {
        $routes = require_once $_SERVER['DOCUMENT_ROOT'].'/app/config/routes.php';
        foreach ($routes as $route=>$params){
            $this->add($route, $params);
        }
    }

    public function run(){

        if($this->match()){
            $path = 'app\controllers\\'.ucfirst($this->params['controller']).'Controller';
            if(class_exists($path)){
                $action = $this->params['action'].'Action';
                if(method_exists($path, $action)){
                   $controller = new $path($this->params);
                   $controller->$action();
                }else{
                    echo 'method '.$action.'not found';
                }
            }else{
                echo 'controller '.$path.'not found';
            }
        }else{
            echo 'bad';
        }
    }

    public function add($route, $params){
        $route = '#^'.$route.'([?=0-9_a-z-A-Z]+){0,}([&=0-9_a-z-A-Z]+){0,}$#';
        $this->routes[$route] = $params;
    }

    /*Проверяю наличие маршрута
     * */
    public function match(){
        $url = trim($_SERVER['REQUEST_URI'], '/');
        //Проверяю если пользователь не авторизирован - перевожу на страницу авторизации
        if(isset($_SESSION['admin']['id']) || !is_bool(strpos($url, 'order/created')) || !is_bool(strpos($url, 'order/put'))
                || !is_bool(strpos($url, 'delovod/goodsbalanse'))
                || !is_bool(strpos($url, 'delovod/downloadorders'))){
        }else{
//            if(count($_SESSION) == 0 || !isset($_POST['action']) || $_POST['action'] == 'login') {
//                unset($_SESSION);
//                $url = 'account/login';
//            }else
            if (isset($_POST['action'])){
                $url = 'account/'.$_POST['action'];
            }
        }
        foreach ($this->routes as $route=>$params){
            if(preg_match($route, $url, $matches)){
                $this->params = $params;
                return true;
            }
        }
        return false;
    }
}