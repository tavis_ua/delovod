<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 27.07.2019
 * Time: 12:10
 */

namespace app\models;


use app\core\Model;

class Prom extends Model
{
    public function getProductsList($products = []){
        $sql = "select prom_id, prom_articul from prom_goods";
        if(count($products)){
            $sql.=" where prom_articul in ('".implode("','", $products)."')";
        }
        return $this->db->getAll($sql);
    }
}