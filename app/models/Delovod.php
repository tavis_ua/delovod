<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 06.08.2019
 * Time: 16:05
 */

namespace app\models;


use app\core\Model;

class Delovod extends Model
{
    public function setLastOrderID($param){
        $fieldName = array_keys($param)[0];
        $sql = "update users set ".$fieldName." =:".$fieldName." where id =:id";
        $data = [
            $fieldName              => $param[$fieldName],
            'id'                    => $param['id']
        ];
        $_SESSION['user_info']->$fieldName = $param[$fieldName];
        $result = $this->db->query($sql, $data);
        return $result;
    }
    /*Устанавливаю статус потребности синхронизации остатков
     * */
    public function setSynchronStatus($params){
        $sql = "select not_sinchronize_articul from users where id =:id";
        $data = [
            "id"    =>$params['id']
        ];
        $result = $this->db->getOne($sql, $data);
        $articles = explode(',', $result->not_sinchronize_articul);
        if($params['synchronstatus'] && !in_array($params['articul'], $articles)){
            $articles[] = $params['articul'];
        }else{
            $index = array_search($params['articul'], $articles);
            if(!is_bool($index)){
                unset($articles[$index]);
            }
        }
        $sql = "update users set not_sinchronize_articul = '".implode(',', $articles )."' where id=:id";
        $data = [
            "id"    =>$params['id']
        ];
        $result = $this->db->query($sql, $data);
        return $result;
    }
}