<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 21.07.2019
 * Time: 18:10
 */

namespace app\models;


use app\core\Model;

class Account extends Model
{
    public function createNewUser(){
        //Проверяю наличие этого пользователя в базе данных
        $sql = "select id from users where login=:login";
        $result = $this->db->query($sql, ['login'=>trim($_POST['login'])]);
        if($result && count($result->fetchAll()) == 0) {

            $data = [
                'login'                 => trim($_POST['login']),
                'pass'                  => md5(trim($_POST['password'])),
                'mail'                  => trim($_POST['email']),
                'delovod'               => trim($_POST['delovod_key']),
                'delovod_version'       => trim($_POST['delovod_version']),
                'horoshop_login'        => $_POST['login-horoshop'],
                'horoshop_pass'         => $_POST['pass-horoshop'],
                'horoshop_project'      => $_POST['horoshop-project-name'],
                'promua'                => $_POST['promua_key'],
                'admin'                 => isset($_POST['admin'])?$_POST['admin']:0,
                'horoshop_order_event'  => isset($_POST['horoshop-order-event']) && $_POST['horoshop-order-event'] == 'on'?1:0
            ];
            if(empty($_SESSION['admin']['id'])) {
                $sql = "insert into users (login, pass, mail, admin, active, tms) 
                  VALUE (:login,:pass,:mail,1,1,now())";
            }else{
                $sql = "insert into users (login, pass, mail, delovod_authorize, delovod_version, horoshop_login, horoshop_pass, horoshop_project, promua_authorize,admin,horoshop_order_event,tms,active)
                    VALUES (:login,:pass,:mail,:delovod,:delovod_version,:horoshop_login,:horoshop_pass,:horoshop_project,:promua,:admin,:horoshop_order_event, now(),1)";
            }

            $this->db->addItem($sql, $data);

        }
    }

    /*Обновляю информацию о пользователе
     * @param array
     * @result bit*/
    public function updateUser($param){
        $pass = empty($param['password'])?'pass':"'".md5($param['password'])."'";

        $sql = "update users set  login=:login, pass=".$pass.", mail=:mail,
            delovod_authorize =:delobod, horoshop_login=:horoshop_login, horoshop_pass =:horoshop_pass, 
            horoshop_project =:horoshop_project, horoshop_order_event=:horoshop_order_event, promua_authorize=:promua, admin=:admin, 
            delovod_storage =:delovod_storage, delovod_version =:delovod_version, promua_last_order_id =:promua_last_order_id, tms=now() where id=:id";
            $data['login']                  = $param['login'];
            $data['mail']                   = $param['email'];
            $data['delobod']                = $param['delovod_key'];
            $data['horoshop_login']         = $param['login-horoshop'];
            $data['horoshop_pass']          = $param['pass-horoshop'];
            $data['horoshop_project']       = $param['horoshop-project-name'];
            $data['horoshop_order_event']   = isset($param['horoshop-order-event']);
            $data['promua']                 = $param['promua_key'];
            $data['admin']                  = isset($param['admin']);
            $data['delovod_storage']        = isset($param['storage'])?json_encode($param['storage']):'';
            $data['delovod_version']        = isset($param['delovod_version'])?$param['delovod_version']:'';
            $data['promua_last_order_id']   = isset($param['promua_last_order_id'])?$param['promua_last_order_id']:'';
            $data['id']                     = $param['id'];
        return $this->db->query($sql, $data);
    }

    /*Ставит метку о том, что пользователь удален
     * @param int
     * @result bit*/
    public function deleteUser($id_usr){
        $sql = "update users set active = 0, tms=now() where id=:id";
        $data['id']=$id_usr;
        return $this->db->query($sql, $data);
    }

    /*Проверка на наличие пользователя в базе данных
     * */
    public function checkUser($data){

    }
}