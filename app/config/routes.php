<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 18.07.2019
 * Time: 12:44
 */

return [
    //Prom.ua
    'prom'  => [
        'controller' => 'prom',
        'action'     => 'index',
    ],
    'prom/index'  => [
        'controller' => 'prom',
        'action'     => 'index',
    ],
    'prom/syncgoods'  => [
        'controller' => 'prom',
        'action'     => 'downloadGoods',
    ],
    //Аккаунты
    'account/login' => [
        'controller' => 'account',
        'action'     => 'login',
    ],
    'account/register' => [
        'controller' => 'account',
        'action'     => 'register',
    ],
    'account/edit' => [
        'controller' => 'account',
        'action'     => 'edit',
    ],
    'account/redirecttomain' => [
        'controller' => 'account',
        'action'     => 'redirecttomain',
    ],
    'account/delete' => [
        'controller' => 'account',
        'action'     => 'delete',
    ],
    'account/update' => [
        'controller' => 'account',
        'action'     => 'update',
    ],
    'account/new' => [
        'controller' => 'account',
        'action'     => 'new',
    ],
    'account/check' => [
        'controller' => 'account',
        'action'     => 'check',
    ],
    'account' => [
        'controller'=>'main',
        'action' => 'index'
    ],
    ''             => [
       'controller'=>'main',
       'action' => 'index'
    ] ,
    '/'             => [
       'controller'=>'main',
       'action' => 'index'
    ] ,
    '/exit'         => [
       'controller'=>'main',
       'action' => 'exit'
    ] ,
    //Отображение общих остатков
    'goods/index'   => [
       'controller'=>'goods',
       'action' => 'index'
    ] ,
    'goods/goods'   => [
       'controller'=>'goods',
       'action' => 'showGoods'
    ] ,
    'goods/load'   => [
       'controller'=>'delovod',
       'action' => 'loadGoods'
    ] ,
    //Деловод
    'delovod/getorderinfo'   => [
       'controller'=>'delovod',
       'action' => 'getOrderInfo'
    ],
    'delovod/downloadorders'  => [
        'controller' => 'delovod',
        'action'     => 'downloadOrders',
    ],
    'delovod/setsynchronstatus'  => [
        'controller' => 'delovod',
        'action'     => 'setSynchronStatus',
    ],
    'order/created'  => [
        'controller' => 'delovod',
        'action'     => 'downloadHoroshopOrder',
    ],
    'order/put'  => [
        'controller' => 'delovod',
        'action'     => 'putOrders',
    ],
    'delovod/goodsbalanse'  => [
        'controller' => 'delovod',
        'action'     => 'CalcGoodsBalance',
    ],
    'horoshop/saveqty'  => [
        'controller' => 'delovod',
        'action'     => 'saveHoroshopQty',
    ],
    'horoshop/downloadcatalog'  => [
        'controller' => 'delovod',
        'action'     => 'downloadCatalog',
    ],


];