<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 18.07.2019
 * Time: 14:53
 */

namespace app\controllers;

use app\core\Controller;

class MainController extends Controller
{
    public function indexAction($mess = ''){
        $sql = "select id, login, mail, tms, `admin` from users where active = 1 order by login";
        $result = $this->db->getAll($sql);
        $out = '<table class="table table-sm table-striped">
            <thead>
                <tr class="table-primary">
                    <th>Логин</th>
                    <th>Email</th>
                    <th>Дата создания/редактирования</th>
                    <th>Администратор</th>
                    <th colspan="3">&nbsp;</th>
                </tr>
            </thead>
            <tbody>';
        foreach ($result as $key=>$item){
            $out .= '<tr id = '.$item->id.'>
                <td>'.$item->login.'</td>
                <td>'.$item->mail.'</td>
                <td>'.date('d.m.y H:i:s', strtotime($item->tms)) .'</td>
                <td> <input id = "'.$item->id.'" type="checkbox" '.($item->admin == 1?'checked = "checked"':'').'></td>
                <td><a href="/account/edit?id='.$item->id.'"><img class="icons" src="/app/views/images/edit.png"></a></td>
                <td><img class="icons" id="deleteBtn" rowid = "'.$item->id.'" src="/app/views/images/delete.png"></td>                
                <td><a href="/goods/index?id='.$item->id.'"><img class="icons" id="goods_'.$item->id.'" rowid = "'.$item->id.'" src="/app/views/images/preview.png"></a></td>                                                                                               
            </tr>';
        }
        $out .= '</tbody></table>';
        $data = [
          'content'=>$out.$mess
        ];
        $this->view->render('Панель администратора', $data);
    }
}