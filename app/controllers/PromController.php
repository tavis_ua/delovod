<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 27.07.2019
 * Time: 12:09
 */

namespace app\controllers;



use app\core\Controller;
use DateTime;

class PromController extends Controller
{

    public function indexAction()
    {
        $this->authorizationAction();
        $group_list = $this->promua_service->make_requestAction('GET', '/cms/settings/stock', NULL);
        $group_list = $this->promua_service->make_requestAction('GET', '/groups/list', NULL);

        $group_html = '<table class="table table-sm table-striped"><thead><tr class="table-primary"><th>Название группы</th></tr></thead><tbody>';
        foreach ($group_list['groups'] as $key => $group_item){
            $group_html .= '<tr><td '.($key == 0?'class="selected"':'').'onclick="getGoods('.$group_item['id'].')">'.$group_item['name'].'</td></tr>';
            if($key == 0) {
                $product_list = $this->promua_service->make_requestAction('GET', '/products/list?limit=20&group_id=' . $group_item['id'], NULL);
            }
        }
        $group_html .= '</tbody></table>';

        $product_html = '<table class="table table-sm table-striped"><thead><tr class="table-primary"><th>Перечень товаров</th></tr></thead><tbody>';
        $data = [
            'group_html' => $group_html,
            'product_html' => $product_html
        ];

        $this->view->render('Остатки', $data);
    }
    

    /*Загрузка кодов соответствия товаров
     * */
    public function downloadGoodsAction(){
        if($this->user_info == null && !empty($_SESSION['user_info'])){
            $this->user_info = $_SESSION['user_info'];
        }else{
            $this->authorizationAction();
        }

        $group_list = $this->promua_service->make_requestAction('GET', '/groups/list?limit=100', NULL);
        while(count($group_list['groups']) > 0) {
            foreach ($group_list['groups'] as $key => $group_item) {
                $product_list = $this->promua_service->make_requestAction('GET', '/products/list?limit=100&group_id=' . $group_item['id'], NULL);

                foreach ($product_list['products'] as $key => $product) {
                    $data = [
                        'prom_id' => $product['id'],
                        'prom_articul' => $product['sku'],
                        'status' => $product['status'],
                    ];
                    $sql = "insert into prom_goods (prom_id,prom_articul,presence)VALUES( :prom_id, :prom_articul, :status)";
                    $result = $this->db->query($sql, $data);
                    if (!$result) {
                        $sql = "update prom_goods set prom_articul =:prom_articul, presence =:status where prom_id =:prom_id";
                        $this->db->query($sql, $data);
                    }

                }
                $last_id = $group_item['id'];
            }
            $group_list = $this->promua_service->make_requestAction('GET', '/groups/list?limit=100&last_id='.$last_id, NULL);
        }
        echo 'ok';
        exit();
    }
}