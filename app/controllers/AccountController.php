<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 21.07.2019
 * Time: 12:35
 */

namespace app\controllers;


use app\core\Controller;


class AccountController extends Controller
{
    /*редактировать данные пользователя
     * */
    public function editAction(){
        $sql = "select * from users where 1 and id=:id and active = 1";
        $data = ['id'=>$_GET['id']];
        $result = $this->db->getOne($sql,$data);
        if($result) {
            $this->view->render('Редактировать', (array)$result);
        }else{
            echo
            '<script> 
                alert("Пользователь не найден"); 
                location.href = "/"; 
            </script>';
        }
    }
    /*авторизация пользователя
     * */
    public function loginAction(){
        if(!isset($_POST) || count($_POST) == 0){
            $this->view->render('Форма авторизации пользователя');
            exit();
        }
        $sql = "select id, admin from users where login = :login and active = 1";
        $data = ['login'=>$_POST['login']];
        $result = $this->db->getOne($sql, $data);
        if(isset($_POST['action']) && $_POST['action'] == 'login' && !$result){
            //Первый запуск системы
            if($_POST['login'] == 'admin' && $_POST['password'] == 'admin'){
                $sql = "select count(*) iCount from users";
                $result = $this->db->getOne($sql);
                if($result->iCount == 0){
                    $data = [
                        'action'=>'register',
                        'admin'=>'1'
                    ];
                    $this->view->render('Необходимо зарегистрировать адниминстратора', $data);
                }
            }
        }else{
            if($result->admin == 1) {
                $_SESSION['admin']['id'] = $result->id;
            }
            if(!isset($_REQUEST['id']) || empty($_REQUEST['id'])){
                $_REQUEST['id'] = $result->id;
            }
            $this->authorizationAction();
            if(!is_bool(strpos($_SERVER['HTTP_REFERER'], 'id='))){
                $domain_name = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'];
                $controllerBorder = strpos($_SERVER['HTTP_REFERER'], '/', strlen($domain_name)+1);
                if(is_bool($controllerBorder)){
                    $main = new MainController(['controller' => 'main', 'action' => 'index']);
                    $main->indexAction();
                }else{
                    $controllerName = substr($_SERVER['HTTP_REFERER'], strlen($domain_name)+1, $controllerBorder-strlen($domain_name)-1);
                    $actionBorder = strpos($_SERVER['HTTP_REFERER'], '/', $controllerBorder+1);
                    if($actionBorder === false){
                        $actionBorder = strpos($_SERVER['HTTP_REFERER'], '?', $controllerBorder+1);
                    }
                    $actionName = substr($_SERVER['HTTP_REFERER'], $controllerBorder+1, $actionBorder-$controllerBorder-1);
                    $path = 'app\controllers\\'.ucfirst($controllerName).'Controller';
                    if(class_exists($path)){
                        $action = $actionName.'Action';
                        if(method_exists($path, $action)){
                            $controller = new $path(['controller' => $controllerName, 'action' => $actionName]);
                            $controller->$action();
                        }else{
                            echo 'method '.$action.'not found';
                        }
                    }else{
                        echo 'controller '.$path.'not found';
                    }
                }
            }else {
                $main = new MainController(['controller' => 'main', 'action' => 'index']);
                $main->indexAction();
            }
        }
    }

    /*Регистрация нового пользователя
     * */
    public function registerAction(){
        $result = $this->model->createNewUser();

        //Если была выполнена регистрация первого пользователя(админа) захожу под его аккаунтом
        if(!isset($_SESSION['admin']['id'])){
            $sql = "select id, login from users";
            $result = $this->db->getOne($sql);
            $_SESSION['admin']['id'] = $result->id;
            $_SESSION['admin']['login'] = $result->login;
        }
        if(!$result){
            $mess = '<script> alert("Операция прервана. Возможно пользователь с таким именем уже существует")</script>';
        }
        $main = new MainController(['controller'=>'main', 'action'=>'index']);
        $main->indexAction($mess);
    }

    /*Подписка на уведомления при создании счета
     * */
    function subscribeOrderEventAction(){
        $this->authorizationAction();
        $data = [
            "token"     =>$_SESSION['user_info']->horoshop_token,
            "event"     => "order_created",
            "target_url"=> 'http://'.$_SERVER['SERVER_NAME']."/order/created?id=".$_SESSION['user_info']->id,
            "url"       => $_SESSION['user_info']->horoshop_project."/api/hooks/subscribe/",
        ];
        $result = $this->Request($data, '', array('Content-Type: application/json'));
        return $result;
    }

    /*Отписка на уведомления при создании счета
     * */
    function unsubscribeOrderEventAction($id){
        $this->authorizationAction();
        $data = [
            "token"     =>  $_SESSION['user_info']->horoshop_token,
            "id"        =>  $id,
            "url"       =>  $_SESSION['user_info']->horoshop_project."/api/hooks/unSubscribe/",
            'target_url'=> 'clt.biz.ua/order/created?id=12'
        ];
        $result = $this->Request($data, '', array('Content-Type: application/json'));
        return $result;
    }

    /*Обновление данных о пользователе
     * */
    public function updateAction(){
        $this->model->updateUser($_POST);
//        $this->unsubscribeOrderEventAction(7);
        if(isset($_POST['horoshop-order-event'])){
            $this->subscribeOrderEventAction();
        }
        $main = new MainController(['controller'=>'main', 'action'=>'index']);
        $main->indexAction();
    }

    /*Удаление пользователя из активных*/
    public function deleteAction(){
        $this->model->deleteUser(empty($_POST['id_usr'])?-1:$_POST['id_usr']);
        echo '1';
    }

    /*Открываю страничку создания нового пользователя
     * */
    public function newAction(){
        $this->view->render('Новый пользователь');
    }

    /*Проверяю наличие пользователя в базе данных
     * */
    public function checkUser(){
        echo $this->model->checkUser($_POST);
    }

    /*Выход на главную страницу с удалением данних о пользователе из сессии
     * */
    public function redirecttomainAction(){
        unset($_SESSION['user_info']);
        echo 'ok';
        exit();
    }
}