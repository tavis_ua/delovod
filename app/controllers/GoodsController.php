<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 05.08.2019
 * Time: 10:50
 */

namespace app\controllers;


use app\core\Controller;

class GoodsController extends Controller
{
    public function indexAction(){
        $this->authorizationAction();
        $out = '<table class="table table-sm table-striped">
            <thead>
                <tr class="table-primary">
                    <th>Артикул</th>
                    <th style="text-align: center">Не<br>синхрон.</th>
                    <th>Название</th>                    
                    <th>"Хорошоп"<img id="download_horoshop" class="icons" title="Загрузить справочник" src="/public/images/download-button.png"></th>
                    <th>"Деловод"</th>
                    <th>"Prom.ua"<img id="download_prom" class="icons" title="Загрузить справочник" src="/public/images/download-button.png"></th>
                    <th colspan="3">&nbsp;</th>
                </tr>
            </thead>
            <tbody>';
        $out .= '</tbody></table>';
        $data = [
            'content' => $out,
            'id_usr'=>isset($_REQUEST['id'])?$_REQUEST['id']:0,
        ];
        $this->view->render('Товары', $data);
    }
    /*Отображает таблицу товаров
     * */
    public function showGoodsAction(){
//        $this->authorizationAction();
        $goods = $this->goodsAction($_REQUEST['onlydifferent'] == "true");
        $notSynchronArticles = $this->model->getUnSynchronArticles();
        $out = '';
        foreach ($goods as $good){
            $out.='<tr id="'.$good['key'].'" '.(isset($good['class']) && !empty($good['class'])?$good['class']:'').'>';
            $out .= '<td>'.$good['key'].'</td>';                                //Артикль
            $out .= '<td><input type="checkbox" '.(in_array($good['key'], $notSynchronArticles)?'checked="checked"':'').'id="'.$good['key'].'" class="synchron-status"></td>';   //Информация о необходимости синхронизации
            $out .= '<td>'.$good['title'].'</td>';                      //Название
            $out .= '<td class="qty" ondblclick="changeQty(\''.$good['key'].'\')">'.$good['horoshop_qty'].'</td>';            //Остаток хорошоп
            $out .= '<td>'.(isset($good['delovod_qty'])?(int)$good['delovod_qty']:0).'</td>';         //Остаток деловод
            $out .= '<td>'.(isset($good['promua_presense'])?$good['promua_presense']:'-').'</td>';     //Статус промюа
            $out .= '<td title="Синхронизировать остатки" onclick="SaveQty(\''.$good['key'].'\', '.(isset($good['delovod_qty'])?(int)$good['delovod_qty']:0).', true)"><img class="icons"  src="/public/images/sync.png">';
            $out .='</tr>';
        }
        echo $out;
        exit();
    }


}