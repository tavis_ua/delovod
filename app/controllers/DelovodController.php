<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 06.08.2019
 * Time: 15:55
 */

namespace app\controllers;


use app\core\Controller;
use stdClass;
use DateTime;
use DateTimeZone;


class DelovodController extends Controller
{
    //Статус заказа
    public function getStateID($title){
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $_SESSION['user_info']->delovod_authorize,
            "action" => "request",
            "params" => [
                "from" => "catalogs.states",
                "fields" => ["id" => "id",
                    "sysName" => "sysName",
                ],
            ]
        ];
        if (!empty($title)){
            $post["params"]["filters"]=[
                    ["alias"=>"sysName",
                    "operator"=>"=",
                    "value"=>$title],
            ];
        }
        $result = json_decode($this->Request($post, 'packet='));
        return $result;
    }
    /*Загрузка первого ИД заказов
     * */
    function getFirstOrdersID(){
        return $this->promua_service->make_requestAction('GET', '/orders/list?limit=1', NULL);
    }

    /*Подсчет остатков и утановка статусов доступности товаров
     * */
    public function CalcGoodsBalanceAction($goods_list = []){
        $balance = [];
        $this->authorizationAction();
        $notSynchronizeArticuls = $this->model->getUnSynchronArticles();
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version"=>"0.15",
            "key"=>$_SESSION['user_info']->delovod_authorize,
            "action"=>"request",
            "params"=>[
                "from"=>[
                    "type"=>"balance",
                    "register"=>"goods",
                    "date"=>date('Y-m-d 00:00:00'),
                ],
                "filters"=>[

                    ["alias"=>"storage",
                        "operator"=>"IL",
                        "value"=>json_decode($this->user_info->delovod_storage)
                    ],
                    ["alias"=>"productNum",
                        "operator"=>"!IL",
                        "value"=>$notSynchronizeArticuls
                    ],
                ],
                "fields"=>["good"=>"good",
                    "good.code"=>"code",
                    "good.productNum"=>"productNum",
                    "storage"=>"storage",
                    "qty"=>"qty"],
                ]
            ];
        if(count($goods_list) > 0) {
            $post["params"]["filters"][] =
                ["alias" => "productNum",
                    "operator" => "IL",
                    "value" => $goods_list,
                ];
        }
        $storageQty = json_decode($this->Request($post, 'packet='));

        foreach ($storageQty as $item){
            if(isset($balance[$item->productNum]))
                $balance[$item->productNum] += $item->qty;
            else
                $balance[$item->productNum] = $item->qty;
        }


        //Устанавливаю статус доступности на проме
        $this->promua_service->setProductStatus($balance);

        //Устанавливаю значение остатков на хорошопе
        $this->horohop_service->saveqtyAction($balance);
        echo 'ok';
        exit();
    }

    public function putOrdersAction(){
        $this->authorizationAction();

        $json = '{
            "order_id":1067,
            "user":1146,
            "delivery_name":"Тест",
            "delivery_email":"test2@gmail.com",
            "delivery_phone":"+38 (097) 805-90-53",
            "delivery_city":"Киев",
            "delivery_address":"тестовая 1",
            "delivery_type":{"id":11,"title":"Доставка курьером от двери до двери"},
            "delivery_price":-1,"comment":"","payment_type":{"id":15,"title":"Оплата при получении"},
            "payment_price":0,"payed":0,"total_default":855,"total_sum":855,"total_quantity":1,
            "discount_percent":0,"discount_value":0,"coupon_code":"","coupon_percent":0,"coupon_discount_value":0,
            "coupon_type":0,"stat_status":1,"stat_created":"2019-09-10 15:40:43","currency":"UAH","order_without_callback":false,
            "products":[{"title":"Городской рюкзак Mark Ryden MR5700 Black","article":"MR_5700BK","price":855,"quantity":1,"discount_marker":"PRICE_OLD","total_price":855}]}';
        $headers = array (
            'Content-Type: application/json'
        );
        $url = 'http://'.$_SERVER['SERVER_NAME'].'/order/created?id='.$_GET['id'];
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }
    /*Загрузка информации о заказе с Хорошопа
     * */
    public function downloadHoroshopOrderAction(){


        /* PUT данные приходят в потоке ввода stdin */
        $putdata = fopen("php://input", "r");

        /* Открываем файл на запись */
        $fp = fopen("myputfile.txt", "w");
        /* Читаем 1 KB данных за один раз
           и пишем в файл */
        while ($data = fread($putdata, 1024))
            fwrite($fp, $data);

        if(!isset($_REQUEST['id']) || empty($_REQUEST['id'])){
            return;
        }

        $this->authorizationAction();
//
//        $post = [
//            "url" => $_SESSION['user_info']->delovod_gate,
//            "version" => "0.15",
//            "key" => $_SESSION['user_info']->delovod_authorize,
//            "action" => "request",
//            "params" => [
//                "from" => "documents.shipment",
//                "fields" => ["id" => "id",
//                    "baseDoc" => "baseDoc",
//                    "deliveryMethod" => "deliveryMethod",
//                    "description" => "description",
//                    "remark" => "remark",
//                ],
//            ]
//        ];
//        $shipment = json_decode($this->Request($post, 'packet='));
//exit();
        $json = file_get_contents("php://input", "r");
//        $json = file_get_contents("http://".$_SERVER['SERVER_NAME']."/myputfile.txt", "r");
        /* Закрываем потоки */
        fclose($fp);
        fclose($putdata);
//        $json = '{
//            "order_id":1067,
//            "user":1146,
//            "delivery_name":"Тест",
//            "delivery_email":"test2@gmail.com",
//            "delivery_phone":"+38 (097) 805-90-53",
//            "delivery_city":"Киев",
//            "delivery_address":"тестовая 1",
//            "delivery_type":{"id":11,"title":"Доставка курьером от двери до двери"},
//            "delivery_price":-1,"comment":"","payment_type":{"id":15,"title":"Оплата при получении"},
//            "payment_price":0,"payed":0,"total_default":855,"total_sum":855,"total_quantity":1,
//            "discount_percent":0,"discount_value":0,"coupon_code":"","coupon_percent":0,"coupon_discount_value":0,
//            "coupon_type":0,"stat_status":1,"stat_created":"2019-09-10 15:40:43","currency":"UAH","order_without_callback":false,
//            "products":[{"title":"Городской рюкзак Mark Ryden MR5700 Black","article":"MR_5700BK","price":855,"quantity":1,"discount_marker":"PRICE_OLD","total_price":855}]}';
        $order = (json_decode($json));

        $data = [
            'code' => $order->user,
            'name'  => trim($order->delivery_name),
            'phone' => trim($order->delivery_phone),
            'email' => trim($order->delivery_email),
        ];

        $data['date']       =  (new DateTime($order->stat_created))->format('Y-m-d H:i:s');
//        $nextNumber         = $this->getOrderNumber();
//        $data['number']     =  'ХО'.substr('00000', 0, 5-strlen($nextNumber)).$nextNumber;
        $data['number']     =  'ХО'.$order->order_id;//TODO: получение номера согласно ИД прома
        $data['firm']       = $this->getFirm();
        //Заказчик
        $person_id = $this->getPerson(array('code'=>$order->user, 'phone'=>trim($order->delivery_phone), 'email'=>trim($order->delivery_email)));

        $data['person'] = $person_id;
        if($person_id == 0){//Добавляю заказчика, если его нет в базе
            $data['delivery_address'] = $order->delivery_city.' '.$order->delivery_address;
            $person_id = $this->savePerson($data);
            if(is_array($person_id)){
                $person_id = 0;
            }
        }
        $data['person'] = $person_id;
        $data['products'] = [];
        $data['amountCost'] = 0;
        foreach ($order->products as $product){
            $data['products'][] = [
                "sku"           => substr('0000', strlen($product->article)).$product->article,
                "quantity"      => $product->quantity,
                "price"         => $product->price,
                "total_price"   => $product->quantity*$product->price,
            ];
            $data['amountCost'] += $product->quantity*$product->price;
        }

        if(isset($order->total_sum) && !empty($order->total_sum)) {
            $data['discount'] = $data['amountCost'] - $order->total_sum;
        }else{
            $data['discount'] = 0;
        }

        $data['currency']   = 1101200000001001;//код для грн.
        $data['rate']       = $this->getCarencyRateAction();
        $data['state']      = 1111500000000005;// Установил значение "Новый" $this->getStateID($order['status'])[0]->id;
        $data['deliveryRemark'] = $order->delivery_city.' '.$order->delivery_address;
        $data['remark']     = $order->comment;
        $data['deliveryMethod'] = $this->getDeliveryMethodsAction($order->delivery_type->title);
        $data['department']     = $this->getDepartment();
        $data['paymentForm']    = $this->getPaymentMethod($order->payment_type->id);

        $result = $this->saveOrder($data);
        $this->model->setLastOrderID(array('horoshop_last_order_id'=>$order->order_id, "id"=>$_SESSION['user_info']->id));
        echo '<pre>';
        var_dump($result);
        echo 'ok';
        exit();
    }

    /*Загрузка информации о заказах с Promua
     * */
    public function downloadOrdersAction(){

        if(!isset($_REQUEST['id']) || empty($_REQUEST['id'])){
            return;
        }
        $this->authorizationAction();
        if(isset($_SESSION['user_info']) && empty($_SESSION['user_info']->promua_basic_order_id)){
            $last_order = $this->getFirstOrdersID()['orders'][0]['id'];
            $sql = "update users set promua_basic_order_id=:last_order_id where id=:id";
            $data = [
                'last_order_id' => $last_order,
                'id' => $_SESSION['user_info']->id
            ];
            $this->db->query($sql, $data);
        }
        if(isset($_SESSION['user_info']) && empty($_SESSION['user_info']->promua_last_order_id)){
            $limit_count = 1;
        }else{
            $limit_count = 100;
        }
//        1100100000001358
        $order_list = $this->promua_service->make_requestAction('GET', '/orders/list?limit='.$limit_count, NULL);
        if(isset($order_list['orders']) && is_array($order_list['orders'])) {
            $fp = fopen("download_prom.log", "a");
            fputs($fp, date('Y.m.d H:i:s') . "\n");
            fclose($fp);
            foreach ($order_list['orders'] as $key => $order) {
                //Прерываю цикл, если ИД заказа = ИД последнего добавленного в базу заказа
                if ($order['id'] == $_SESSION['user_info']->promua_last_order_id) {
                    break;
                }
                $data = [
                    'code' => $order['client_id'],
                    'name' => trim($order['client_last_name']) . ' ' . trim($order['client_first_name']) . ' ' . trim($order['client_second_name']),
                    'phone' => trim($order['phone']),
                    'email' => trim($order['email']),
                ];

                $data['date'] = (new DateTime($order['date_created']))->setTimezone(new DateTimeZone('Europe/Kiev'))->format('Y-m-d H:i:s');
//                $nextNumber = $this->getOrderNumber('ПР'); //TODO: получение номера согласно ИД прома
//                $data['number'] = 'ПР' . substr('00000', 0, 5 - strlen($nextNumber)) . $nextNumber;
                $data['number'] = 'ПР'.$order['id'];
                $data['firm'] = $this->getFirm();
                //Заказчик
                $person_id = $this->getPerson(array('phone' => $order['phone'], 'email' => $order['email']));
                $data['person'] = $person_id;
//            if($person_id == 0){//Добавляю заказчика, если его нет в базе
                $data['delivery_address'] = $order['delivery_address'];
                $person_id = $this->savePerson($data);
                if (is_array($person_id)) {
                    $person_id = 0;
                }
//            }

                $data['products'] = $order['products'];
                $data['person'] = $person_id;
                $data['discount'] = 0;/*TODO добавили дисконт*/
                $data['amountCost'] = preg_replace('/[^0-9]/', '', $order['price']);
                $data['currency'] = 1101200000001002;//Установил гривну
                $data['rate'] = $this->getCarencyRateAction();
                $data['state'] = 1111500000000005;// Установил значение "Новый" $this->getStateID($order['status'])[0]->id;
                $data['deliveryRemark'] = $order['delivery_address'];
                $data['remark'] = $order['client_notes'];
                $data['deliveryMethod'] = $this->getDeliveryMethodsAction($order['delivery_option']['name']);
                $data['department'] = $this->getDepartment();
                $data['paymentForm'] = $this->getPaymentMethod($order['payment_option']);
                $this->saveOrder($data);
                $this->model->setLastOrderID(array('promua_last_order_id' => $order_list['orders'][0]['id'], "id" => $_SESSION['user_info']->id));
            }
        }else{
            $fp = fopen("error_download_prom.log", "a");
            fputs($fp, date('Y.m.d H:i:s') .$order_list. "\n");
            fclose($fp);
        }
    }

    public function saveOrder($data){

        if(count($data['products']) > 0) {
            $post["params"]["tableParts"]["tpGoods"] = [];
        }
        $articule_list=[];
        foreach ($data['products'] as $key=>$product){
            $articule_list[]= substr('0000', strlen($product['sku'])).$product['sku'];
        }
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $_SESSION['user_info']->delovod_authorize,
            "action" => "request",
            "params" => [
                "from" => "catalogs.goods",
                "fields" => ["id" => "id",
                    "productNum" => "productNum",
                ],
                "filters"=>[
                    ["alias"=>"productNum",
                        "operator"=>"IL",
                        "value"=>$articule_list
                    ],
                ],
            ]
        ];
        $products = json_decode($this->Request($post, 'packet='));

        //Преобразую в двумерный массив
        foreach ($products as $key=>$item){
            $products[$item->productNum] = $item->id;
            unset($products[$key]);
        }

        $storages = json_decode($_SESSION['user_info']->delovod_storage);
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $_SESSION['user_info']->delovod_authorize,
            "action" => "saveObject",
            "params" => [
                "header" => [
                    "id"               => !isset($data['id'])||empty($data['id'])?"documents.saleOrder":$data['id'],
                    "date"             => $data['date'],
                    "number"           => $data['number'],
                    "firm"             => $data['firm'],
                    "storage"          => $storages[count($storages)-1],
                    "amountCur"        => $data['amountCost'],
                    "person"           => $data['person'],
                    "rate"             => $data['rate'],
                    "currency"         => $data['currency'],
                    "state"            => $data['state'],
                    "remarkFromPerson" => $data['remark'],
//                    "discount"         => 200.5,/*FIXME: проверить ссылку*/
                    "department"       => $data['department'],
                    "paymentForm"      => $data['paymentForm'],
                ],
            ],
        ];
        if(isset($_SESSION['user_info']) && (int)$_SESSION['user_info']->delovod_version == 9){//FIXME:Не используется в 10-той версии
            $post["params"]["header"]["remarkFromPerson"] = $data['deliveryRemark'];
            $post["params"]["header"]["deliveryMethod"] = $data['deliveryRemark'];
        }
        foreach ($data['products'] as $key=>$product){
            $post["params"]["tableParts"]["tpGoods"][]=[
                "rowNum"    =>  $key+1,
                "good"      =>  $products[$product['sku']],
                "qty"       =>  $product['quantity'],
                "unit"      =>  1103600000000001, //Постоянное системное значение
                "amountCur" =>  preg_replace('/[^0-9]/','', $product['total_price']),
                "price"     =>  preg_replace('/[^0-9]/','', $product['price']),
//                "goodType"  =>  1004000000000014, //Постоянное системное значение не используется в 10-той версии
            ];
        }

        //Бананс на хорошопе
        $balance = $this->horohop_service->getBalance($articule_list);

        //Рассчетный остаток
        foreach ($data['products'] as $key=>$product){
            if(isset($balance[$product['sku']])){
                $balance[$product['sku']] = $balance[$product['sku']] - $product['quantity'];
            }else{
                $balance[$product['sku']] = 0;
            }
            if($balance[$product['sku']] < 0){
                $balance[$product['sku']] = 0;
            }
        }
        $this->promua_service->setProductStatus($balance);
        $this->horohop_service->saveqtyAction($balance);

        $json = json_encode($post);
        $result = json_decode($this->Request($post, 'packet='));

        if (isset($result->id)){
            //Проверяю наличие информации в базе о методе доставки для заказа
            $post = [
                "url" => $_SESSION['user_info']->delovod_gate,
                "version" => "0.15",
                "key" => $_SESSION['user_info']->delovod_authorize,
                "action" => "request",
                "params" => [
                    "from" => "documents.shipment",
                    "fields" => ["id" => "id",
                        "baseDoc" => "baseDoc",
                    ],
                    "filters"=>[
                        ["alias"=>"baseDoc",
                            "operator"=>"=",
                            "value"=>$result->id
                        ],
                    ],
                ]
            ];
            $shipment = json_decode($this->Request($post, 'packet='));
            if(is_array($shipment) && isset($shipment[0]->id)){
                $shipment_id = $shipment[0]->id;
            }

            //Сохраняю информацию о методе доставки
            $post = [
                "url" => $_SESSION['user_info']->delovod_gate,
                "version" => "0.15",
                "key" => $_SESSION['user_info']->delovod_authorize,
                "action" => "saveObject",
                "params" => [
                    "header" => [
                        "id"               => !isset($shipment_id)||empty($shipment_id)?"documents.shipment":$shipment_id,
                        "date"             => $data['date'],
                        "deliveryMethod"   => $data['deliveryMethod'],
                        "remark"           => $data['remark'],
                        "baseDoc"          => $result->id,
                        "description"      => $data['deliveryRemark'],
                    ],
                ],
            ];
            $result_shipment = json_decode($this->Request($post, 'packet='));

        }
        return $result;
    }

    /*Загружаю существующие категории товаров/услуг
     * */
    public function getDelovodCatalog($articule_list = []){
        $this->authorizationAction();
        //Загружаю категории с деловода
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $this->user_info->delovod_authorize,
            "action" => "request",
            "method" => "get",
            "params" => [
                "from" => "catalogs.goods",
                "fields" => ["id" => "id",
                    "name" => "name",
                    "sysName" => "sysName",
                    "version" => "version",
                    "parent" => "parent",
                    "productNum" => "productNum",
                    "accPolicy" => "accPolicy",
                ],
            ]
        ];
        if(count($articule_list)) {
            $post["params"]["filters"] = [
                ["alias" => "productNum",
                    "operator" => "IL",
                    "value" => $articule_list
                ],
            ];
        }
        $result = json_decode($this->Request($post, 'packet='));
        return $result;
    }

    /*Загрузка каталога из хорошопа в деловод
     * */
    public function downloadCatalogAction(){
        $this->authorizationAction();
//        $post = [
//            "url" => $_SESSION['user_info']->delovod_gate,
//            "version" => "0.15",
//            "key" => $this->user_info->delovod_authorize,
//            "action" => "request",
//            "method" => "get",
//            "params" => [
//                    "from" => "catalogs.units",
//                    "fields" => ["id" => "datid",
//                        "name" => "name",
//                    ],
//            ]
//        ];
//        $result = json_decode($this->Request($post, 'packet='));
        $this->setStartBalance();
        //Загружаю категории с деловода
        $delovod_goods = $this->getDelovodCatalog();
        $categories = $this->horohop_service->getCategories();

        //Проставляю ссылки соответствия каталогов хорошопа и деловода
        foreach ($categories as $key=>$item){
            if($item['parent'] == 1){
                $categories[$key]['ru'] = 'Товары';
                $categories[$key]['uk'] = 'Товари';
                $item['ru'] = 'Товары';
                $item['uk'] = 'Товари';
            }
            if(is_array($delovod_goods)) {
                foreach ($delovod_goods as $dev_key => $dev_item) {
                    if (($item['parent'] == 1 && $dev_item->parent == 0 ||
                            trim($item['uk']) == trim($dev_item->name)) && !isset($item['dev_index'])
                    ) {//Устанавливаю ИД категории товаров
                        $categories[$key]['dev_index'] = $dev_item->id;
                        break;
                    }
                }
            }
        }
        foreach ($categories as $key=>$item){
            if($item['parent'] != 0){
                $post = [
                    "url" => $_SESSION['user_info']->delovod_gate,
                    "version" => "0.15",
                    "key" => $_SESSION['user_info']->delovod_authorize,
                    "action" => "saveObject",
                    "method" => "post",
                    "params" => [
                        "header" => [
                            "id"     => isset($item['dev_index']) && !empty($item['dev_index'])?$item['dev_index']:"catalogs.goods",
                            "name"   => array("ru"=>$item['ru'],"uk"=>$item['uk']),
                            "accPolicy"   => '1201200000001003',
                            "isGroup" => 1
                        ]
                    ],
                ];
                if(isset($categories[$item['parent']]['dev_index']) && !empty($categories[$item['parent']]['dev_index'])){
                    $post['params']['header']['parent'] = $categories[$item['parent']]['dev_index'];
                }
                $result = json_decode($this->Request($post, 'packet='));
                if(isset($result->id) && !empty($result->id)){
                    $categories[$key]['dev_index'] = $result->id;
                }
            }
        }
        //Список товаров хорошопа
        $horoshop_goods = $this->horohop_service->getBalance([], false);

        //Получаю список товаров, отфильтрованных по артикульному номеру
        $delovod_goods = $this->getDelovodCatalog(array_keys($horoshop_goods));
        foreach ($delovod_goods as $key=>$item){
            $delovod_goods[$item->productNum] = $item;
            unset($delovod_goods[$key]);
        }

        //Сохраняю товары хорошопа в деловод
        foreach ($horoshop_goods as $key=>$item){
            if(isset($delovod_goods[$key])){
                $item['dev_index'] = $delovod_goods[$key]->id;
            }
            if(isset($item['dev_index']) && !empty($item['dev_index'])) {
                $id = $item['dev_index'];
            }else{
                $id = "catalogs.goods";
            }
//            $id=  isset($item['dev_index']) && !empty($item['dev_index'])?$item['dev_index']:"catalogs.goods";
            $post = [
                "url" => $_SESSION['user_info']->delovod_gate,
                "version" => "0.15",
                "key" => $_SESSION['user_info']->delovod_authorize,
                "action" => "saveObject",
                "method" => "post",
                "params" => [
                    "header" => [
                        "id"     => isset($item['dev_index']) && !empty($item['dev_index'])?$item['dev_index']:"catalogs.goods",
                        "name"   => array("ru"=>$item['name']['ru'],"uk"=>(empty($item['name']['uk'])?$item['name']['ru']:$item['name']['uk'])),
                        "parent"  => $categories[$item['parent']]['dev_index'],
                        "productNum" => $key,
                        "accPolicy"   => '1201200000001003',
                        "mainUnit"   => '1103600000000001',
                    ]
                ],
            ];
            $result = json_decode($this->Request($post, 'packet='));
            $horoshop_goods[$key]['dev_index'] = $result->id;
            $post = [
                "url" => $_SESSION['user_info']->delovod_gate,
                "version" => "0.15",
                "key" => $this->user_info->delovod_authorize,
                "action" => "saveObject",
                "method" => "post",
                "params" => [
//                    "from" => "informationRegisters.goodsPrices",
//                    "fields" => ["date" => "date",
//                        "good" => "good",
//                        "priceType" => "priceType",
//                        "currency" => "currency",
//                        "price" => "price",
//                    ],
                    "header" => [
                        "id"     => "informationRegisters.goodsPrices",
                        "date"=>date('Y-m-d 00:00:00'),
                        "good"   => $result->id,
                        "currency"  => '1101200000001001',
                        "priceType" => '1101300000001001',
                        "price"   => $item['price'],
                    ]
                ]
            ];
            $result = json_decode($this->Request($post, 'packet='));
        }

        //Ввод первичных остатков
        $this->setStartBalance($horoshop_goods);
    }

    /*Ввод первичных остатков
     * */
    public function setStartBalance($goods = []){
        if(count($goods)) {
            $post = [
                "url" => $_SESSION['user_info']->delovod_gate,
                "version" => "0.15",
                "key" => $_SESSION['user_info']->delovod_authorize,
                "action" => "saveObject",
                "method" => "post",
                "params" => [
                    "header" => [
                        "id" => "documents.startBalance",
                        "date" => date('Y-m-d H:i:s'),
                        "number" => "1",
                        "firm" => "1100400000001001",
                        "account" => "1119000000001015",
                        "storage" => "1100700000000001",
                        "currency" => "1101200000001001",
//                        "costItem" => "1106100000000007",
                    ]
                ],
            ];
            $post["params"]["tableParts"]["tpBalances"] = [];
            foreach ($goods as $key=>$good){
                $post["params"]["tableParts"]["tpBalances"][] = [
                    "rowNum" => count($post["params"]["tableParts"]["tpBalances"]) + 1,
                    "good" => $good['dev_index'],
                    "unit" => '1103600000000001', //шт
                    "qty"  => $good['quantity'],
                ];
            }
            $result = json_decode($this->Request($post, 'packet='));
        }
    }
    /*Установка построчного значения остатков в хорошопе
     * */
    public function saveHoroshopQtyAction(){
        $data [$_REQUEST['article']] = $_REQUEST['qty'];
        $this->horohop_service->saveqtyAction($data);
        $this->promua_service->setProductStatus([$_REQUEST['article']=>$_REQUEST['qty']]);
        echo 'ok';
        exit();
    }

    /*Установка статуса необходимости синхронизации остатков
     * */
    public function setSynchronStatusAction(){
        $this->authorizationAction();
        $this->model->setSynchronStatus($_REQUEST);
        echo 'ok';
        exit();
    }

    /*Добавляю заказчика в базу деловода
     * */
    public function savePerson($data){
        $input ["names"]=[
            ["pr"=>["ru"=>$data['name'], "uk"=>$data['name']], "kind"=>"fullName"],
        ];
//        {"names":[{"pr":{"ru":"Чижова АНна Викторовна","uk":"Чижова АНна Викторовна"},"kind":"fullName"},{"pr":{"uk":"Чижова АНна Викторовна"},"kind":"fullName"}],"phones":[{"pr":"0631611966","kind":"phone"}],"emails":[],"codes":[],"addresses":[],"certificates":[],"dates":[],"messengers":[],"urls":[],"attributes":[],"notes":[]}
        $json = '';
        if(!empty(trim($data['phone']))){
            $input["phones"] = [
                ["pr"=>$this->changeMaskPhoneNumber($data['phone']),
                    "kind"=>"phone"]
            ];
        }
        if(!empty(trim($data['email']))){
            $input["emails"]=[
                ["pr"=>$data['email'],
                "kind"=>"email"]
            ];
        }
        if(isset($data['delivery_address']) && !empty(trim($data['delivery_address']))){
            $input["addresses"] = [
                ["pr"=>($data['delivery_address']),
                    "kind"=>"legalAddress"]
            ];
        }
//        {"names":[{"pr":{"ru":"name"},"kind":"fullName"}],"phones":[{"pr":"38-063-161-19-66","kind":"phone"}],"addresses":{"pr":"111","kind":"legalAddress"}}
        $json = json_encode($input);
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $_SESSION['user_info']->delovod_authorize,
            "action" => "saveObject",
            "method" => "post",
            "params" => [
                "header" => [
                    "id"     => isset($data['person']) && !empty($data['person'])?$data['person']:"catalogs.persons",
                    "name"   => array("ru"=>$data['name'],"uk"=>$data['name']),
                    "phone"  => $this->changeMaskPhoneNumber($data['phone']),
                    "email"  => $data['email'],
                    "personType"  => 1004000000000035, //Физическое лицо
                    "details" => $json
                ]
            ],
        ];

        $result = json_decode($this->Request($post, 'packet='));

        if(isset($result->id) && !empty($result->id))
            return $result->id;
        else
            return $result;
    }

    /*Возвращаю ИД заказчика из деловода
     *@param array информация о телефоне или  */
    public function getPerson($data = []){
        $this->authorizationAction();
        //Загружаю фирмы с деловода
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $this->user_info->delovod_authorize,
            "action" => "request",
            "method" => "get",
            "params" => [
                "from" => "catalogs.persons",
                "fields" => ["id" => "id",
                    "name" => "name",
                    "phone" => "phone",
                    "email" => "email",
                    "code" => "code",
                    "details" => "details",
                ],
            ]
        ];
            if (!empty($data['email'])) {
                $post["params"]["filters"][] = ['alias' => 'email', 'operator' => '%', "value" => $data['email']];
            } elseif (!empty($data['phone'])) {
                $post["params"]["filters"][] = ['alias' => 'phone', 'operator' => 'IL', "value" => [str_replace('+', ' ', $data['phone']), $this->changeMaskPhoneNumber($data['phone'])]];
            }
            $result = json_decode($this->Request($post, 'packet='));

        if(count($result)){
            return $result[0]->id;
        }else{
            return 0;
        }
    }

    function changeMaskPhoneNumber($phone){
        $phone = preg_replace('/^(\+38){0,}|^(38){0,}|^(8){0,}|[\+ \-\(\)]/','', $phone);
        return $phone;
    }

    //Загружаю фирмы с деловода
    public function getFirm(){
        $this->authorizationAction();
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $this->user_info->delovod_authorize,
            "action" => "request",
            "params" => [
                "from" => "catalogs.firms",
                "fields" => ["id" => "id",
                    "name" => "name",
                ],
            ]
        ];
        $result = json_decode($this->Request($post, 'packet='));
        $keyFirm = 0;//CLUBTECH - 0 ФЛП Горобец - 1 смотреть нужно по key, который возвращается в $result
        return json_decode($this->Request($post, 'packet='))[$keyFirm]->id;
    }

    /*Способы доставки
     * */
    public function getDeliveryMethodsAction($name){
        $this->authorizationAction();
        $out = [
            'Новой почтой' => '1110400000001001',
            'Новою поштою' => '1110400000001004',
            'Самовивіз'  => '1110400000000001',
            'Самовывоз'  => '1110400000000001',
            'Новая Почта'=> '1110400000000001',
            'Доставка курьером от двери до двери'=> '1110400000001001',
            'Доставка кур\'єром від дверей до дверей'=> '1110400000001001',
            'Укрпоштою'    => '1110400000001003',
            'Укрпочтой'    => '1110400000001003',
            'Автолюкс'    => '1110400000001005',
            'Интайм'    => '1110400000001006',
            'Деливери'    => '1110400000001007',
        ];
        //Загружаю метод доставки с деловода
//        $post = [
//            "url" => $_SESSION['user_info']->delovod_gate,
//            "version" => "0.15",
//            "key" => $this->user_info->delovod_authorize,
//            "action" => "request",
//            "params" => [
//                "from" => "catalogs.currency",
//                "fields" => ["id" => "id",
//                    "name"  =>"name",
//                ]
//            ]
//        ];
//        $result = json_decode($this->Request($post, 'packet='));

        return $out[$name];
    }
    //Курс валют
    public function getCarencyRateAction(){
        $this->authorizationAction();

        //Загружаю курс с деловода
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $this->user_info->delovod_authorize,
            "action" => "request",
            "params" => [
                "from" => "informationRegisters.currencyRates",
                "fields" => ["id" => "id",
                    "date"  =>"date",
                    "currencyFrom" => "currencyFrom",
                    "currencyTo" => "currencyTo",
                    "rate" => "rate",
                ]
            ]
        ];
        $result = json_decode($this->Request($post, 'packet='));
        return $result[count($result)-1]->rate;
//1101200000001001 дол
//1101200000001002 грн
//1114800000000003 тип
    }
    //Подразделение
    public function getDepartment($name = 'Основний підрозділ'){
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $_SESSION['user_info']->delovod_authorize,
            "action" => "request",
            "params" => [
                "from" => "catalogs.departments",
                "fields" => ["id" => "id",
                    "name" => "name",

                ],
//                "filters"=>[
//                    ["alias"=>"name",
//                        "operator"=>"=",
//                        "value"=>$name
//                    ],
//
//                ]
            ]
        ];
        $info = json_decode($this->Request($post, 'packet='));
        return $info[0]->id;
    }

    /*Получение метода оплаты
     *@param array //Способ оплаты с прома
     *@return int //Способ оплаты деловода*/
    public function getPaymentMethod($payment_option = null){
//        $result = $this->horohop_service->getHoroshopPaymentMethod();
//        $result = $this->promua_service->getPromuaPaymentMethod();
//        $post = [
//            "url" => $_SESSION['user_info']->delovod_gate,
//            "version" => "0.15",
//            "key" => $this->user_info->delovod_authorize,
//            "action" => "request",
//            "params" => [
//                "from" => "catalogs.paymentForms",
//                "fields" => ["id" => "id",
//                    "name"  =>"name",
//                    "code"  =>"code",
//                    "delMark"  =>"delMark",
//                ]
//            ]
//        ];
//        $result = json_decode($this->Request($post, 'packet='));
        $payment_method = [
            '6303996' =>'',// Vasa, Mastercard - WayForPay
            '14' =>'1110300000001002',// Vasa, Mastercard - WayForPay
            '6161576' =>'1110300000001002',// Наложенный платеж Новая Почта ??
            '160445'  =>'1110300000000001',// Безнал
            '12'  =>'1110300000000001',// Безнал
            '160446'  =>'1110300000001002',// Наложенный платеж
            '15'  =>'1110300000001001',// Наложенный платеж/Оплата при получении
            '16'  =>'1110300000001002',// Приват 24
            '17'  =>'1110300000001002',// Приват 24
            '160444'  =>'1110300000000002',// Наличными
            '13'  =>'1110300000000002',// Наличными
        ];
        if($payment_option != null){
            return $payment_method[$payment_option];
        }
        return null;
    }

    /*Получаю номер заказа
     * */
    public function getOrderNumber($prefix){
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $_SESSION['user_info']->delovod_authorize,
            "action" => "request",
            "params" => [
                "from" => "documents.saleOrder",
                "fields" => ["id" => "id",
                    "number" => "number",
                ],
                "filters"=>[
                    ["alias"=>"number",
                        "operator"=>"%",
                        "value"=>$prefix
                    ],
                ],
            ]
        ];
        $result = json_decode($this->Request($post, 'packet='));
        $maxNumber = 0;
        foreach ($result as $key =>$item){
            $number = (int)preg_replace('/'.$prefix.'|^[0-9]/','', $item->number);
            $number > $maxNumber? $maxNumber = $number: false;
        }
        return ++$maxNumber;
    }

    public function getOrderInfoAction(){
//        $this->getOrderNumber('ПР');
        $this->authorizationAction();
//        $result = $this->getFirm();
//        $post = [
//            "url" => $_SESSION['user_info']->delovod_gate,
//            "version" => "0.15",
//            "key" => $this->user_info->delovod_authorize,
//            "action" => "getObject",
//            "params" => [
//                "id" => "1109100000001802",
//            ]
//        ];
        $post = [
            "url" => $_SESSION['user_info']->delovod_gate,
            "version" => "0.15",
            "key" => $_SESSION['user_info']->delovod_authorize,
            "action" => "request",
            "params" => [
                "from" => "documents.saleOrder",
                "fields" => ["id" => "id",
                    "date" => "date",
                    "person" => "person",

                ],
//                "filters"=>[
//                    ["alias"=>"number",
//                        "operator"=>"%",
//                        "value"=>"ПР"
//                    ],
//                ],
            ]
        ];
        $info = json_decode($this->Request($post, 'packet='));
        return $info;
    }
}

//Примеры запроса к деловоду

// Список валют
//        $post = [
//            "url" => $_SESSION['user_info']->delovod_gate,
//            "version" => "0.15",
//            "key" => $this->user_info->delovod_authorize,
//            "action" => "request",
//            "params" => [
//                "from" => "catalogs.currency",
//                "fields" => ["id" => "id",
//                    "name" => "name",
//
//                ],
//            ]
//        ];
//        $list = json_decode($this->Request($post, 'packet='));

// Баланс с фильтрацией по складам
//        $post = [
//            "url" => $_SESSION['user_info']->delovod_gate,
//            "version"=>"0.15",
//            "key"=>"7UTGVLl5ztVkhXKEidquT29rGhR6w3",
//            "action"=>"request",
//            "params"=>[
//                "from"=>[
//                    "type"=>"balance",
//                    "register"=>"goods",
//                    "date"=>date('Y-m-d 00:00:00'),
//
//                ],
//                "filters"=>[
//                    ["alias"=>"productNum",
//                        "operator"=>"=",
//                        "value"=>"1543",
//                    ],
//                    ["alias"=>"storage",
//                        "operator"=>"IL",
//                        "value"=>["1100700000000001","1100700000001002"]
//                    ],
//                ],
//                "fields"=>["good"=>"good",
//                    "good.code"=>"code",
//                    "good.productNum"=>"productNum",
//                    "storage"=>"storage",
//                    "qty"=>"qty"],
//                ]
//            ];
//        $storageQty = json_decode($this->Request($post, 'packet='));