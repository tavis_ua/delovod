<link rel="stylesheet" href="/public/styles/login.css">
<div id="authorize-block">
    <div id="title"><?php echo $title?></div>
    <form action="/account" id="autorize-form" method="post">
        <input type="hidden" name="action" value="<?=empty($action)?'login':$action?>">
        <?php if(isset($admin) && $admin == 1){?>
            <input type="hidden" name="admin" value="1">
        <?php }?>
        <table>
            <?php if(isset($action) && $action == 'register'){?>
                <tr>
                    <td class="title">
                        <span>Введите email</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="email">
                    </td>
                </tr>
            <?php }?>
            <tr>
                <td class="title">
                    <span>Введите логин</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="login">
                </td>
            </tr>
            <tr>
                <td>
                    <span>Введите пароль</span>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <input type="password" name="password">
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Войти</button>
                </td>
            </tr>
        </table>
    </form>
</div>