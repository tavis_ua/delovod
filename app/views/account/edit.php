<link rel="stylesheet" href="/public/styles/login.css">
<script src="/public/scripts/accounts.js"></script>
<div id="authorize-block">
    <div id="title"><?php echo $title?></div>
    <form action="/account/update" id="autorize-form" method="post">
        <input type="hidden" name="action" value="update">
        <input type="hidden" name="id" value="<?php echo empty($id)?0:$id?>">
        <table>

            <tr>
                <td  colspan="3" class="title">
                    <span>Email пользователя</span>
                </td>
            </tr>
            <tr>
                <td colspan="3" tabindex="0">
                    <input type="text" name="email" value="<?php echo $mail?>">
                </td>
            </tr>

            <tr>
                <td  colspan="3" class="title">
                    <span>Логин</span>
                </td>
            </tr>
            <tr>
                <td colspan="3" tabindex="1">
                    <input type="text" name="login" value="<?php echo $login?>">
                </td>
            </tr>
            <tr>
                <td colspan="3" >
                    <span>Пароль</span>
                </td>
            </tr>
            <tr>
                <td  colspan="3" class="title" tabindex="2">
                    <input type="password"  name="password" value="">
                </td>
            </tr>
            <tr>
                <td  colspan="3" class="title" tabindex="2">
                    <span>Администратор</span> <input type="checkbox" <? echo (!empty($admin)?(($admin == 1?'checked = "checked"':'')):'')?> id="admin" name="admin">
                </td>
            </tr>
            <tr>
                <td colspan="3">Параметры авторизации</td>
            </tr>
            <tr class="authorize-block title table-primary">
                <td>horoshop.ua</td>
                <td>delovod.ua</td>
                <td>prom.ua</td>
            </tr>
            <tr class="authorize-block">
                <td>Логин</td>
                <td>Ключ магазина</td>
                <td rowspan="2">Ключ магазина</td>
            </tr>
            <tr class="authorize-block">
                <td tabindex="3"><input id="login-horoshop" name="login-horoshop" value="<?php echo !empty($horoshop_login)?$horoshop_login:'' ?>"></td>
                <td><input name="delovod_key" id="delovod_key" tabindex="4" value="<?php echo !empty($delovod_authorize)?$delovod_authorize:'' ?>"></td>
            </tr>
            <tr class="authorize-block">
                <td>Пароль</td>
                <td>Версия базы данных</td>
                <td rowspan="5"><input type="text" name="promua_key" id="promua_key" value="<?php echo !empty($promua_authorize)?$promua_authorize:'' ?>"></td>
            </tr>
            <tr class="authorize-block">
                <td tabindex="4"><input id="pass-horoshop"  name="pass-horoshop" value="<?php echo !empty($horoshop_pass)?$horoshop_pass:'' ?>"></td>
                <td><select name="delovod_version" id="delovod_version"><option selected = "selected">9</option><option>10</option></select></td>
            </tr>
            <tr class="authorize-block">
                <td>Название проекта</td>
                <td>Склад реализации товара</td>
            </tr>
            <tr class="authorize-block">
                <td tabindex="3"><input id="horoshop-project-name" name="horoshop-project-name" value="<?php echo !empty($horoshop_project)?$horoshop_project:'' ?>"></td>
                <td style="vertical-align: top" id="delovod_storage" rowspan="2"><img class="wait" src="/public/wait.gif" alt="Загрузка..."></td>
            </tr>
            <tr class="authorize-block">
                <td tabindex="3">Синхронизировать остатки при формировании заказа<input id="horoshop-order-event" name="horoshop-order-event" type="checkbox" <?php echo !empty($horoshop_order_event)?'checked="checked"':'' ?>></td>
            </tr>
            <tr>
                <td colspan="3">
                    <button type="submit" tabindex="10">Сохранить</button>
                </td>
            </tr>
        </table>
    </form>
</div>
<script>
    $("#Save").on('click', function () {
        $('form').submit(validForm());
    })

    $('#delovod_key').on('change', function (event) {

        if($('#delovod_key').val().substr(0, 30).length == 30){
            getStorage($('#delovod_key').val());
        }
    })
    $(document).ready(function(){
        getStorage('<?php echo !empty($delovod_authorize)?$delovod_authorize:'' ?>');
        $('#delovod_version').val(<?echo !empty($delovod_version)?$delovod_version:'9'?>)
//        var params = {
//            "login": "api",
//            "password": "q4lrfb3"
//        }
//        $.ajax({
//            url:'//clubtech.in.ua/api/auth/',
//            type: "GET",
//            contentType: "application/json",
//            charset:"windows-1251",
//            data:params,
//            success:function (result) {
//                console.log(result);
//            }
//        })
    });
    $('input').on('change', function () {
        $(this).removeClass('errorField');
    })
</script>