<link rel="stylesheet" href="/app/views/main/index.css">
<div id="tool-bar">
    <button id="create-new-user">Добавить пользователя</button>
    <button id="exit">Выйти</button>
</div>
<p>Список пользователей</p>

<?php echo $content;?>
<!--Use https://api.delovod.ua endpoint for v10 databases-->
<script>
    //Удаление пользователя
    $(document).delegate('.deleteBtn', 'click', function () {
        if(confirm('Удалить информацию о пользователе?')){
            var param = {
                id_usr:$(this).attr('rowid')
            };
            $.ajax({
                url:'/account/delete',
                data:param,
                type:'POST',
                success:function (result) {
                    if(result == 1){
                        console.log($('tr#'+param.id_usr));
                        $('tr#'+param.id_usr).remove();
                    }else{
                        alert('Во время удаления возникла ошибка. Обратитесь к разработчику');
                    }
                }
            })
        }
    });

    $('#exit').on('click', function () {
        location.href = '/account/login';
    })

    $('#create-new-user').on('click', function () {
        location.href = '/account/new';
    })

</script>
