<link rel="stylesheet" href="/app/views/horoshop/index.css">
<div id="tool-bar">
    <button id="load-goods" onclick="mainPage()">Главная страница</button>
<!--    <button id="load-goods">Загрузить товары</button>-->
<!--    <button id="synchron-goods">Синхронизировать остатки</button>-->
    <button id="prev-page" onclick="prevPage()">Предыдущая страница</button>
    <button id="next-page" onclick="nextPage()">Следующая страница</button>
</div>

<?php echo $content;?>

<script>
    function mainPage() {
        location.href = '/';
    }
    $(document).ready(function () {
        window.page = <?php echo isset($_SESSION['horoshop']['page'])?$_SESSION['horoshop']['page']:1?>;
        loadGoods(<?php echo $id_usr;?>, window.page);
    })

    /*Загружаю страницу товаров*/
    function loadGoods(id_usr, page) {
        $('tbody').html('Загрузка...');
        var params = {
            page:page,
            id:id_usr
        }
        $.ajax({
            url: '/horoshop/goods',
            data:params,
            chache:false,
            type:'post',
            success:function (html) {
                $('tbody').html(html);
            }
        })
    }

    /*Предыдущая страница*/
    function prevPage() {
        if(window.page > 1){
            window.page--;
            loadGoods(<?php echo $id_usr;?>, window.page);
        }else{
            alert('Это первая страница товаров');
        }
    }

    //Следующая страница
    function nextPage() {
        window.page++;
        loadGoods(<?php echo $id_usr;?>, window.page);
    }

    //Изменение остатков вручную
    function changeQty(article) {
        var inputField = '<input class="editField" onkeyup="editFieldKeyUp('+article+')" id="input_" value="'+$(event.target).html()+'">';
        $(event.target).html(inputField);
        $("#input_"+article).select();
    }

    function editFieldKeyUp(article) {
        if($.inArray(event.keyCode, [13, 27]) >= 0){
            if(event.keyCode == 13 && confirm('Отправить изменение на сервер?')){
                SaveQty(article, $(event.target).val());
            }
            $(event.target).parent()[0].innerHTML = $(event.target).val();
        }
    }

    /*Отправляет значение остатков на сервер хорошоп
    * */
    function SaveQty(article, qty, ask = false) {
        if(!ask || confirm('Отправить значение остатков на сервер?')) {
            var params = {
                article: article,
                qty: qty,
                id:<?php echo $id_usr;?>
            }
            $.ajax({
                url: '/horoshop/saveqty',
                data: params,
                cahce: false,
                type: 'post',
                success: function (result) {
                    console.log(result);
                }
            });
        }
    }

</script>

<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 01.08.2019
 * Time: 12:18
 */
