<link rel="stylesheet" href="/app/views/goods/index.css">
<img id=wait src="/public/images/wait.gif">
<div id="tool-bar">
    <button id="load-goods" onclick="mainPage()">Главная страница</button>
<!--    <button id="load-goods">Загрузить товары</button>-->
<!--    <button id="synchron-goods">Синхронизировать остатки</button>-->
    <button id="prev-page" class="navigatorBtn" onclick="prevPage()">Предыдущая страница</button>
    <button id="next-page" class="navigatorBtn" onclick="nextPage()">Следующая страница</button>
    <button id="sync-goods" onclick="sinchrinizeGoods()">Синхронизировать остатки</button>
    <span>Отображать только товары с разбежностями в остатках</span> <input type="checkbox" id="show_different"
                                                                            onclick="loadGoods(<?php echo $id_usr;?>, window.page, document.getElementById('show_different').checked)">

    <table id="header">
    </table>
</div>
<form action="/goods/load" method="post" id="post-file" enctype="multipart/form-data">
    <input type="file" name="goods" id="horoshop-goods" style="display: none" accept=".xls,.xlsx">
</form>
<div id="content">
    <?php echo $content;?>
</div>
<script>
    function mainPage() {
        $.ajax({
            url:'/account/redirecttomain',
            success:function (result) {
                if(result == 'ok'){
                    location.href = '/';
                }
            }
        })
    }
    $(document).ready(function () {
        $('#header').html($('.table')[0].getElementsByTagName('thead')[0].innerHTML);
        window.page = <?php echo isset($_SESSION['horoshop']['page'])?$_SESSION['horoshop']['page']:1?>;
        loadGoods(<?php echo $id_usr;?>, window.page);
    })
    /*Синхронизировать остатки
    * */
    function sinchrinizeGoods() {
        if(confirm('Синхронизировать остатки?')) {
            $('#wait').show();
            $.ajax({
                url: 'http://<?echo $_SERVER['SERVER_NAME']?>/delovod/goodsbalanse?id=<?echo $_SESSION['user_info']->id?>',
                success: function (result) {
                    location.href = location.href;
                }
            })
        }
    }

    $('#horoshop-goods').change(function () {
        console.log($('#horoshop-goods').val());
        $('#post-file').submit();
    });

    /*Синхронизация таблиц деловода и промюа
     * */
    $(document).delegate("body", "click", function(){
        let field = $('.editField')[0];
        if(field != undefined) {
            $(field).parent()[0].innerHTML = $(field).val();
        }
    })
    /*Загружаю товары с хорошопа
    * */
    $(document).delegate("#download_horoshop", "click", function(){
        if(confirm("Загрузить справочники категорий и товаров?")){
            $.ajax({

            })
//            $('#horoshop-goods').trigger('click');
        }
        alert('1');
    });
    /*Загружаю товары с промюа
    * */
    $(document).delegate("#download_prom", "click", function(){
        $('#wait').show();
        var param = {
            id:"<?php echo $_REQUEST['id']?>"
        };
        $.ajax({
            url:'/prom/syncgoods',
            data:param,
            success:function () {
                location.href = location.href;
            }

        })
    });

    /*Устанавливаю/снимаю значение атрибута "не синхронизировать" стовара
    * */
    $(document).delegate('.synchron-status', 'click', function (e) {

        var param = {
            id:"<?php echo $_REQUEST['id']?>",
            articul:e.target.id,
            synchronstatus:e.target.checked
        }
        $.ajax({
            url:'/delovod/setsynchronstatus',
            data:param,
            success:function (result) {
                console.log(result);
            }
        })
    })

    /*Загружаю страницу товаров*/
    function loadGoods(id_usr, page, only_different = false) {
        $('#wait').show();
        $('tbody').html('Загрузка...');
        console.log(only_different)
        if(only_different){
            $('.navigatorBtn').attr("disabled", "disabled");
        }else{
            $('.navigatorBtn').removeAttr("disabled");
        }
        var params = {
            id:id_usr,
            page:page,
            onlydifferent:only_different
        }
        $.ajax({
            url: '/goods/goods',
            data:params,
            chache:false,
            type:'get',
            success:function (html) {
                $('tbody').html(html);
                $.each($($('tr.table-primary')[1]).find('th'), function (key, element) {
                    let th_0 = $($('tr.table-primary')[0]).find('th')[key];
                    let th_1 = $($('tr.table-primary')[1]).find('th')[key];
                    $(th_0).css('width', $(th_1).width());
                });
                $('#wait').hide();
            }
        })
    }

    /*Предыдущая страница*/
    function prevPage() {
        if(window.page > 1){
            window.page--;
            loadGoods(<?php echo $id_usr;?>, window.page);
        }else{
            alert('Это первая страница товаров');
        }
    }

    //Следующая страница
    function nextPage() {
        window.page++;
        loadGoods(<?php echo $id_usr;?>, window.page);
    }

    //Изменение остатков вручную
    function changeQty(article) {

        var inputField = '<input class="editField" onkeyup="editFieldKeyUp(\''+article+'\')" id="input_" value="'+$(event.target).html()+'">';
        $(event.target).html(inputField);
        $("#input_").select();
    }

    function editFieldKeyUp(article) {
        if($.inArray(event.keyCode, [13, 27]) >= 0){
            if(event.keyCode == 13 && confirm('Отправить изменение на сервер?')){
                SaveQty(article, $(event.target).val());
            }
            $(event.target).parent()[0].innerHTML = $(event.target).val();
        }
    }

    /*Отправляет значение остатков на сервер хорошоп
    * */
    function SaveQty(article, qty, ask = false) {
        if(!ask || confirm('Отправить значение остатков на сервер?')) {
            var params = {
                article: article,
                qty: qty,
                id:<?php echo $id_usr;?>
            }
            $.ajax({
                url: '/horoshop/saveqty',
                data: params,
                cahce: false,
//                type: 'post',
                success: function (result) {
                    alert(result);
                }
            });
        }
    }

</script>

<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 01.08.2019
 * Time: 12:18
 */
