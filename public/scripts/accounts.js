/**
 * Created by tavis on 27.07.2019.
 */

/*Проверка формы на правильность введенных данных
* */
function validForm() {
    $.each($('input'), function (key, item) {
        if($(item).attr('type') != 'checkbox' && $(item).val() == ""){
            $(item).addClass('errorField');
        }
    })
    return $('.errorField').length == 0;
}

/*Получение списка мест хранения
* */
function getStorage(key){
    if(key.length != 30){
        return false;
    }
    let param = {
        key:key
    }
    $.ajax({
        url:'/app/lib/storages.php',
        data:param,
        cache:false,
        success:function (json) {
            console.log(json);
            let out = '<div id="storage-list">';

            $.each(JSON.parse(json), function (key, item) {
                out += '<div class="storage-row"><div>'+item.name+'&nbsp;</div><div><input id="'+item.id+'" name="storage[]"' +
                    (item.storage_on != undefined? 'checked="checked"':'')+' type="checkbox" value="'+item.id+'"></div></div>';
            })
            out += '</div>';
            $('#delovod_storage').html(out);
        }
    })
}