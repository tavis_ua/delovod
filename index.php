<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 18.07.2019
 * Time: 10:41
 */
use app\core\Route;

require_once 'app/lib/dev.php';

spl_autoload_register(function ($class){
    $file_class = str_replace('\\', '/', $class).'.php';
    if(file_exists($file_class)){
        require_once $file_class;
    }
});

session_start();

$router = new Route();
$router->run();
exit();